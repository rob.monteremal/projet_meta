package dataset;

public class Edge {
	private int node1;
	private int node2;
	private double duedate;
	private int length;
	private int capacity;

	
	public Edge(int node1, int node2, double duedate, int length, int capacity) {
		this.setNode1(node1);
		this.setNode2(node2);
		this.setDuedate(duedate);
		this.setLength(length);
		this.setCapacity(capacity);
	}


	public int getNode1() {
		return node1;
	}


	public void setNode1(int node1) {
		this.node1 = node1;
	}


	public int getNode2() {
		return node2;
	}


	public void setNode2(int node2) {
		this.node2 = node2;
	}


	public double getDuedate() {
		return duedate;
	}


	public void setDuedate(double duedate) {
		this.duedate = duedate;
	}


	public int getLength() {
		return length;
	}


	public void setLength(int length) {
		this.length = length;
	}


	public int getCapacity() {
		return capacity;
	}


	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

}
