package dataset;

import java.util.ArrayList;
import java.util.Collections;

public class Resource {
	private int id;
	private ArrayList<Task> tasks = new ArrayList<Task>();
	private double capacity;
	private double length;
	private double dueDate;
	
	public Resource(int id, double capacity, double dueDate, double length)	{
		this.setId(id);
		this.setCapacity(capacity);
		this.setDueDate(dueDate);
		this.setlength(length);
	}

	public void pushTask(Task task)	{
		this.tasks.add(task);
	}
	
	public Task removeLastTask(Task task) {
		return this.tasks.remove(this.tasks.size()-1);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the capacity
	 */
	public double getCapacity() {
		return capacity;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	/**
	 * @return the length
	 */
	public double getlength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setlength(double length) {
		this.length = length;
	}

	/**
	 * @return the dueDate
	 */
	public double getDueDate() {
		return dueDate;
	}
	
	public ArrayList<Task> getTasks(){
		return this.tasks;
	}

	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(double dueDate) {
		this.dueDate = dueDate;
	}
	
	/*
	 * Updates the tasks associated to the resource
	 * an update consists of advancing each task of one stage
	 */
    public void updateTasks(ArrayList<Task> tasks, int evacNode, int node1, int node2)    {
        for (Task t : tasks) {
            if (t.getIdnode() != evacNode & t.getIdnode()==node1) {
                t.incrementShift((int) this.getlength());
                t.setIDNode(node2);
            }
        }
    }

	/*
	 * Returns a sorted list of doubles containing the time each task ends
	 */
	public ArrayList<Double> getTasksEnd(double shift)	{
		ArrayList<Double> end_times = new ArrayList<Double>();
		for (Task t : this.getTasks())	{
			end_times.add(t.getEvactime());
		}
		end_times.add(shift);
		Collections.sort(end_times);
		
		return end_times;
	}
}
