package dataset;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Solution {
	private ArrayList<Task> originaltasks = new ArrayList<Task> (); //Tasks when at their starting node
	private ArrayList<Task> tasks = new ArrayList<Task> (); //Tasks when at the safe node
	private ArrayList<Path> paths;
	private ArrayList<Resource> resourcesCrossed = new ArrayList<Resource>();
	private String instancepath;
	private double evacuationtime = 0;
	private long computeTime = 0;
	private int id;
	private static int solutionCount;
	
	/*
	 * First constructor of the Solution class, gives one solution when given an instance.
	 */
	public Solution(String instancepath) {
		this.setInstancepath(instancepath);		
		Read.checker = new Checker();
		List<String> lines = Read.ReadFile(instancepath);
		Read.UpdatenodeMatrix(lines);

		for (int i = 0; i < Read.getNbOfPaths(lines); i++) {
			// Creates one path
			String[] pathString = lines.get(i + 2).split(" ");
			int node1 = new Integer(pathString[0]);
			Task task = new Task(i, node1, new Integer(pathString[2]), 0);
			task.setNbPeople(new Integer(pathString[1]));
			task.setDuration();
			
			tasks.add(task);
			originaltasks.add(new Task(task));
			
			for (int j = 0; j < pathString.length; j++) {
				int node2 = new Integer(pathString[j]);

				this.matchTaskToResource(lines, tasks, node1, node2);
				node1 = node2;
			}

		}
		
		this.setEvactime(tasks);
	}
	
	
	/*
	 * Second constructor of the Solution class, gives the best solution when given a list of paths.
	 */
	public Solution(ArrayList<Path> paths_, String instancepath, int id) {
		this.paths = paths_;
		long t_start = System.currentTimeMillis();
		this.setId(id);
		this.setInstancepath(instancepath);		
		Read.checker = new Checker();
		List<String> lines = Read.ReadFile(instancepath);
		Read.UpdatenodeMatrix(lines);

		for (int i = 0; i < Read.getNbOfPaths(lines); i++) {
			// Creates one path
			Path path = paths.get(i);
			int node1 = path.getEdges().get(0).getNode1();
			Task task = new Task(i, node1, path.getMaxrate(), 0);
			task.setNbPeople(path.getNbpeople());
			task.setDuration();
			
			tasks.add(task);
			if(Checker.debug) {
				System.out.println(task);
				System.out.println(tasks.get(tasks.size()-1));
			}
			originaltasks.add(new Task(task));
			int originalShift = originaltasks.get(originaltasks.size()-1).getShift();
			
			for (int j = 0; j < path.getNbofnodes(); j++) {
				int node2 = path.getEdges().get(j).getNode2();
				if(Checker.debug)	{
					for (Task t : tasks)	{
						System.out.print("|E"+(t.getId()+1)+" s= "+t.getShift());
					}
					System.out.println();
					for (Task t : originaltasks)	{
						System.out.print("|E"+(t.getId()+1)+" or_s= "+t.getShift());						
					}
					System.out.println();
				}
				resourcesCrossed.add(this.matchTaskToResource(lines, tasks, node1, node2));
				int newShift = originaltasks.get(originaltasks.size()-1).getShift();
				for (Resource r : resourcesCrossed)	{
					if (newShift - originalShift > 0)	{
						for (Task t : r.getTasks())	{
							if (t.getId() == task.getId())	{
								t.incrementShift(newShift - originalShift);
								if(Checker.debug) {System.out.println("UPDATED TASK"+(t.getId()+1)+" with "+(newShift - originalShift));}
							}
						}
						originalShift = newShift;
					}
				}
				node1 = node2;
			}
			// empties the list
			resourcesCrossed = new ArrayList<Resource>();
		}

		this.setEvactime(tasks);
		this.setComputeTime(System.currentTimeMillis() - t_start);
	}

	/*
	 * FindSolution gives the best solution when we permute the order of the tasks by 1.
	 * Tasks here represented by their paths.
	 * Tests n*(n-1)/2 unique neighbouring solutions.
	 */
	public static Solution FindSolutionSwap(String instancepath) {
		ArrayList<Path> paths=Read.getPaths(Read.ReadFile(instancepath));
		Solution bestSolution = new Solution(paths, instancepath, 0);
		int solutionCount = 0;
		for(int i=0; i<(paths.size()-1); i++) {
			for(int j=i+1; j<paths.size(); j++)	{
				solutionCount++;
				Collections.swap(paths, i, j);
				Solution currentSolution = new Solution(paths, instancepath, solutionCount);
				if(Checker.debug) {currentSolution.printSolution();}
				if (currentSolution.getEvacuationtime() < bestSolution.getEvacuationtime()) {
					bestSolution = currentSolution;
				}
				Collections.swap(paths, j, i);
			}
		}
		return bestSolution;
	}
	
	/*
	 * FindSolution gives the best solution by trying every permutation
	 * Tasks here represented by their paths.
	 * Tests n! (every) unique neighbouring solutions.
	 * FACTORIAL COMPLEXITY -- HIGH COMPUTE TIME
	 */
	public static Solution FindSolutionAll(String instancepath) {
		ArrayList<Path> paths = Read.getPaths(Read.ReadFile(instancepath));
		Solution bestSolution = new Solution(paths, instancepath, 0);
		long solutionTotal = Solution.factorial(paths.size());
		Solution.solutionCount = 0;
		
		Solution.permuteSolution(paths, 0, solutionTotal, bestSolution, instancepath);

		return bestSolution;
	}

	/*
	 * Computes every permutation of a solution
	 * Is used in FindSolutionAll
	 */
	private static void permuteSolution(ArrayList<Path> paths, int index, long solutionTotal, Solution bestSolution, String instancepath){
		//If we are at the last element - nothing left to permute
	    if(index >= paths.size() - 1) {
			Solution.solutionCount++;
			Solution currentSolution = new Solution(paths, instancepath, solutionCount);
			// one every 1000 solution is printed
			if (Solution.solutionCount % 1000 == 0)	{
				System.out.println("Solution "+Solution.solutionCount+"/"+solutionTotal);
//				currentSolution.printSolution();
			}
			if (currentSolution.getEvacuationtime() < bestSolution.getEvacuationtime()) {
				bestSolution = currentSolution;
			}
			
	    	return;
	    }

	    //For each index in the sub array paths[index...end]
	    for(int i = index; i < paths.size(); i++) {
	        //Swap the elements at indices index and i
			Collections.swap(paths, i, index);

	        //Recurse on the sub array arr[index+1...end]
	        Solution.permuteSolution(paths, index+1, solutionTotal, bestSolution, instancepath);

	        //Swap the elements back
			Collections.swap(paths, index, i);
	    }
	}
	
	/*
	 * Core of the checker :
	 * it tries a solution given the ordered task list
	 */
	public Resource matchTaskToResource(List<String> lines, ArrayList<Task> tasks_, int node1, int node2) {
		// The Task that we're trying to fit is not added to the Resource yet
		Task task = tasks_.remove(tasks_.size()-1);
		Resource r = Read.createResource(lines, tasks_, node1, node2);
		
		int originalShift = task.getShift();
		for (double time : r.getTasksEnd(originalShift))	{
			if (time < originalShift)	{
				continue;
			}
			// Task gets updated
			task.setShift((int)time);
			r.pushTask(task);
			if(Checker.debug) {System.out.println("time = "+time);}
			
			// If the solution works it is retained
			if(Read.checker.checkResource(r))	{
				tasks.add(new Task(task));
				originaltasks.get((originaltasks.size() - 1)).incrementShift((int)time - originalShift);
				r.updateTasks(tasks, Read.getEvacNode(lines), node1, node2);
				if(Checker.debug) {System.out.println("E"+(task.getId()+1)+" works with shift set as "+originaltasks.get((originaltasks.size() - 1)).getShift());}
				return r;
				
			// Solution doesn't work, it gets deleted
			} else {
				r.removeLastTask(task);
				if(Checker.debug) {System.out.println("Doesn't work with shift set as "+time);}
			}
		}
		return r;
	}
	
	/*
	 * Quick recursive factorial calculation
	 * Can't calculate above 20! as the result doesn't fit in a long
	 */
	public static long factorial(int n) {
	    if (n > 20) throw new IllegalArgumentException(n + " is out of range");
	    return (1 > n) ? 1 : n * factorial(n - 1);
	}
	
	/*
	 * Sets the evacuation time of the solution as the maximum evac time of any task
	 */
	public void setEvactime(ArrayList<Task> tasks)	{
		this.setEvacuationtime(tasks.get(0).getDuration()+tasks.get(0).getShift());
		for (Task t : tasks)	{
			if (t.getEvactime() > this.getEvacuationtime())	{
				this.setEvacuationtime(t.getEvactime());
			}
		}
	}

	/*
	 * Prints the Solution nicely
	 */
	public void printSolution()	{
		System.out.println("Solution #"+this.getId()+":");
		for(Task t : this.getOriginalTasks()) {
			System.out.println("Starting node #"+t.getIdnode()+" with rate = "+t.getRate()+" and delay = "+t.getShift());
		}		System.out.println("Compute time: "+this.getComputeTime()+" ms");
		System.out.println("Evacuation time = "+this.getEvacuationtime());
		System.out.println();
	}
	
	/*
	 * Writes the solution to a file with the same name
	 */
	public void writeToFile(String method)	{
		try {
			String instance = instancepath.substring(0, instancepath.length() -5).substring(10);
			PrintWriter pw = new PrintWriter(new FileWriter("Solutions/"+method+"_"+instance+".full"));
			pw.println(instance);	// name of the associated instance file
			pw.println(this.paths.size());	// number of paths
			
			for(Path p : paths)	{
				for(Task t : this.getOriginalTasks())	{
					if (p.getEdges().get(0).getNode1() == t.getIdnode())	{
						pw.println(t.getIdnode()+" "+t.getRate()+" "+t.getShift());						
					}
				}
			}
			
			pw.println("valid");
//			pw.println(this.getEvacuationtime()); // evacuation time
			pw.println(this.computeTime);	// time to solve (in ms)
			pw.print(method);	// method used to solve
			
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/*
	 * Setters and getters
	 */
	public ArrayList<Task> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}

	public ArrayList<Task> getOriginalTasks() {
		return originaltasks;
	}

	public void setOriginalTasks(ArrayList<Task> tasks) {
		this.originaltasks = tasks;
	}
	
	public String getInstancepath() {
		return instancepath;
	}

	public void setInstancepath(String instancepath) {
		this.instancepath = instancepath;
	}

	public double getEvacuationtime() {
		return evacuationtime;
	}

	public void setEvacuationtime(double evacuationtime) {
		this.evacuationtime = evacuationtime;
	}


	public long getComputeTime() {
		return computeTime;
	}

	public void setComputeTime(long computeTime) {
		this.computeTime = computeTime;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

}
