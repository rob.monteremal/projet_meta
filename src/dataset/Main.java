package dataset;

public class Main {
	
	public static void main(String[] args) {

		String filename = "dense_10_30_3_5_I.full";
		String method = "neighbor_swap";
		String instancepath = "Instances/"+filename;
		
		Solution solution = Solution.FindSolutionSwap("Instances/"+filename);
		System.out.println("\n=== BEST SOLUTION ===\n");
		solution.printSolution();
		solution.writeToFile("neighbor_swap");
	
		//Checker
		new Checker("Solutions/"+method+"_"+filename);

		// Test checker only
		System.out.println("Lower bound: " + Read.computeLowerBound(instancepath));
		System.out.println("Upper bound: " + Read.computeUpperBound(instancepath));

	}

}
