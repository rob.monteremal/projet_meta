package dataset;

import java.util.*;

public class Path {
	private int nbpeople;
	private int maxrate;
	private int nbofnodes;
	private ArrayList<Edge> edges= new ArrayList<Edge>();
	
	public Path(int nbpeople, int maxrate, int nbofnodes) {
		this.setNbpeople(nbpeople);
		this.maxrate = maxrate;
		this.setNbofnodes(nbofnodes);
	}

	public int getNbpeople() {
		return nbpeople;
	}

	public void setNbpeople(int nbpeople) {
		this.nbpeople = nbpeople;
	}

	public int getMaxrate() {
		return maxrate;
	}

	public void setMaxrate() {
		this.maxrate = Read.findMinCapacity(this);
	}

	public int getNbofnodes() {
		return nbofnodes;
	}

	public void setNbofnodes(int nbofnodes) {
		this.nbofnodes = nbofnodes;
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	public void pushEdges(Edge edge) {
		edges.add(edge);
	}
	
}
