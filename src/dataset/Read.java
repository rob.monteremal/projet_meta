package dataset;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Read {
	public static int[][] nodeMatrix; // Creates a 2D matrix (nodes, nodes, values)
	public static int nbOfNodes;
	public static int nbOfEdges;
	public static Checker checker;
//	public static Task[] tasks;
	public static ArrayList<Task> tasks = new ArrayList<Task> ();
	public static ArrayList<Resource> resources = new ArrayList<Resource>();

		
	/*
	 *Reads a file and return it as a list of strings
	 */
	public static List<String> ReadFile(String filename) {
		try {
			List<String> lines = Files.readAllLines(Paths.get(filename));
			return lines;
		} catch (IOException e) {
			System.err.format("ReadFile error.");
			e.printStackTrace();
			return null;
		}		
	}
	
	
	/*
	 * Update the NodeMatrix
	 */
	public static void UpdatenodeMatrix(List<String> lines) {
		// Reads the number of nodes and number of edges
		int nbOfPaths = Read.getNbOfPaths(lines);
		String[] dataLine = lines.get(nbOfPaths + 3).split(" ");
		Read.nbOfNodes = new Integer(dataLine[0]);
		Read.nbOfEdges = new Integer(dataLine[1]);
		//System.out.println("Number of nodes: " + nbOfNodes + ", number of edges: " + nbOfEdges);

		Read.nodeMatrix = new int[Read.nbOfNodes + 1][Read.nbOfNodes + 1];
		// Reads every edge
		for (int i = 0; i < Read.nbOfEdges; i++) {
			String edge[] = lines.get(i + nbOfPaths + 4).split(" ");
			int node1 = new Integer(edge[0]);
			int node2 = new Integer(edge[1]);

			// Saves the line of the edge
			Read.nodeMatrix[node1][node2] = i + nbOfPaths + 4;
			Read.nodeMatrix[node2][node1] = i + nbOfPaths + 4;
		}
	}
	
	
	/*
	 * Create an array of path using the instance file
	 */
	public static ArrayList<Path> getPaths(List<String> lines) {
		int nbOfPaths = Read.getNbOfPaths(lines);
		Read.UpdatenodeMatrix(lines);
		ArrayList<Path> Paths = new ArrayList<Path>();
		
		for (int i = 0; i < nbOfPaths; i++) {
			// Creates one path		
			String[] pathString = lines.get(i + 2).split(" ");		
			int nbofpeople = new Integer(pathString[1]);
			int maxrate = new Integer(pathString[2]);
			int nbofnodes = new Integer(pathString[3]);
			int node1 = new Integer(pathString[0]);
			Path path = new Path(nbofpeople, maxrate, nbofnodes);
			
			for (int j=4; j < (nbofnodes+4); j++) {
				//Create a new edge to add to the current path
				int node2 = new Integer(pathString[j]);
				int ref = Read.nodeMatrix[node1][node2];
				
				String[] line = lines.get(ref).split(" ");
				double duedate = Double.parseDouble(line[2]);
				int length = new Integer(line[3]);
				int capacity = new Integer(line[4]);
				
				Edge edge = new Edge(node1, node2, duedate, length, capacity);
				path.pushEdges(edge);
				
				node1 = node2;
			}
			/*System.out.println("Path n°"+(i+1)+" that have "+path.getNbpeople()+" of people, and "+path.getNbofnodes()+" edges.");
			for(Edge e : path.getEdges())
			{
				System.out.println("From "+e.getNode1()+" to "+e.getNode2()+" with "+e.getCapacity()+" capacity...");
			}*/
			path.setMaxrate();
			Paths.add(path);
		}
		return Paths;
	}
	
	
	/*
     *Check the solution file to see if it is valid
     */
	public static void checkFile(String filename) {
		try {
			List<String> lines = ReadFile(filename);

			// Reads the number of paths and evac node
			String[] dataLine = lines.get(1).split(" ");
			int nbOfPaths = new Integer(dataLine[0]);
			int evacNode = new Integer(dataLine[1]);
			System.out.println("Number of paths: " + nbOfPaths + ", evacuation node: " + evacNode);

			Read.UpdatenodeMatrix(lines);
			// Reads every path
			for (int i = 0; i < nbOfPaths; i++) {
				// Creates one path
				String[] pathString = lines.get(i + 2).split(" ");
				int node1 = new Integer(pathString[0]);
				
				for (int j = 4; j < pathString.length; j++) {
					int node2 = new Integer(pathString[j]);
					
					Resource r = Read.createResource(lines, Read.tasks, node1, node2);

					if (checker.checkResource(r)) {
						//Update each task
						r.updateTasks(Read.tasks, evacNode, node1, node2);
					} else {
						System.err.format("checkResource error.");
						return;
					}

					node1 = node2;
				}
			}
		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", filename);
			e.printStackTrace();
		}
		
	}
	
	
	/*
	 * Returns the number of paths
	 */
	public static int getNbOfPaths(List<String> lines)	{
		String[] dataLine = lines.get(1).split(" ");
		return new Integer(dataLine[0]);
	}
	
	/*
	 * Returns the safe node
	 */
	public static int getEvacNode(List<String> lines)	{
		String[] dataLine = lines.get(1).split(" ");
		return new Integer(dataLine[1]);
	}
	
	
	
	/*
	 * Calculates the min capacity of a path
	 */
	public static int findMinCapacity(Path p)	{
		int min = Integer.MAX_VALUE;
		for (Edge e : p.getEdges())	{
			int cap = e.getCapacity();
			min = Integer.min(min, cap);
		}
		return min;
	}
	
	
	/*
	 * Calculates a lower bound for the evacuation time
	 * The lower bound is the evacuation time for the longest path
	 */
	public static int computeLowerBound(String instancePath)	{
		int lowerBound = -1;
		List<String> lines = Read.ReadFile(instancePath);
		
		for (Path p : Read.getPaths(lines))	{
			int evacTime = p.getNbpeople() / p.getMaxrate();
			for (Edge e : p.getEdges())	{
				evacTime += e.getLength();
			}
			lowerBound = Integer.max(evacTime, lowerBound);
		}
		
		return lowerBound;
	}

	
	/*
	 * Calculates an upper bound for the evacuation time
	 * The upper bound is sum of the evac time for each path
	 */
	public static int computeUpperBound(String instancePath)	{
		int upperBound = 0;
		List<String> lines = Read.ReadFile(instancePath);
		
		for (Path p : Read.getPaths(lines))	{
			int evacTime = p.getNbpeople() / p.getMaxrate();
			for (Edge e : p.getEdges())	{
				evacTime += e.getLength();
			}
			upperBound += evacTime;
		}
		
		return upperBound;
	}
	
	/*
	 * Creates a resources and adds the necessary tasks to it
	 */
	public static Resource createResource(List<String> lines, ArrayList<Task> tasks_, int node1, int node2)	{
		Resource r;
		int ref = Read.nodeMatrix[node1][node2];

		//If the resource hasn't already been created
		if (ref >= 0) {
			String[] line = lines.get(ref).split(" ");
			r = new Resource(resources.size(), new Double(line[4]), new Double(line[2]),
					new Double(line[3]));
			Read.nodeMatrix[node1][node2] = 0-r.getId();
			//Creates a resource
			Read.resources.add(r);

		} else {
			r = Read.resources.get(0-ref);
		}
		for (Task t : tasks_) {
			if (t.getIdnode() == node1) {
				r.pushTask(new Task(t));
			}
		}
		
		return r;
	}
}
