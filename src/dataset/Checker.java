package dataset;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class Checker {

	public static boolean debug = false;
	private String solutionpath;
	private String instancepath;

	/*
	 * First constructor, simply creates an object
	 */
	public Checker() {
	}
	
	/*
	 * Second constructor, creates an object, then check if the solution is feasable
	 */
	public Checker (String solution) {
		System.out.println("=== CHECKER ENGAGED ===");
		solutionpath = solution;
		this.getInstanceName();

		System.out.println("= Checking for instance: "+instancepath+" its solution: "+solutionpath+" =");
		this.checknbtasks();
		Read.tasks = getTasks();

		try {
			List<String> lines = Read.ReadFile(instancepath);

			// Reads the number of paths and evacnode
			String[] dataLine = lines.get(1).split(" ");
			int nbOfPaths = new Integer(dataLine[0]);
			int evacNode = new Integer(dataLine[1]);
			System.out.println("Number of paths: " + nbOfPaths + ", evacuation node: " + evacNode);

			Read.UpdatenodeMatrix(lines);
			// Reads every path
			for (int i = 0; i < nbOfPaths; i++) {
				// Creates one path
				String[] pathString = lines.get(i + 2).split(" ");
				int node1 = new Integer(pathString[0]);
				
				for (int j = 4; j < pathString.length; j++) {
					int node2 = new Integer(pathString[j]);
					
					Resource r = Read.createResource(lines, Read.tasks, node1, node2);

					if (this.checkResource(r)) {
						//Update each task
						r.updateTasks(Read.tasks, evacNode, node1, node2);
					} else {
						System.err.format("Checker: checkResource error.");
						return;
					}

					node1 = node2;
				}
			}
			System.out.println("File is valid!");

		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", instancepath);
			e.printStackTrace();
		}
		//Finaly we check if the escape time is valid
		/*if(this.checkDuration()) {
			System.out.println("File is valid!");
		}*/
	}
	

	public String getInstanceName() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(solutionpath));
			String line;
			line = reader.readLine();
			reader.close();
			this.instancepath="Instances/"+line+".full";
			return line;
		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", solutionpath);
			e.printStackTrace();
			return null;
		}
	}

	private int checknbtasks() {
		try {
			int nbtaskssol = Integer.parseInt(Files.readAllLines(Paths.get(solutionpath)).get(1));
			String lineinstance = Files.readAllLines(Paths.get("Instances/" + this.getInstanceName() + ".full"))
					.get(1);
			String nbtasksinstance = lineinstance.split(" ")[0];
			if (nbtaskssol == Integer.parseInt(nbtasksinstance)) {
				return nbtaskssol;
			} else {
				System.err.format("Error: Number of tasks invalid.");
				return -1;
			}
		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", solutionpath);
			e.printStackTrace();
			return -1;
		}
	}
	
	public ArrayList<Task> getTasks() {
		try {
			int i;
			int nbtasks = this.checknbtasks();
			ArrayList<Task> tasks = new ArrayList<Task>();
			for (i = 0; i<nbtasks; i++) { //Creating an array of tasks
				String tasksinfo = Files.readAllLines(Paths.get(solutionpath)).get(i+2);
				String infopeople = Files.readAllLines(Paths.get(instancepath)).get(i+2);
				int nbpeople = new Integer(infopeople.split(" ")[1]);
				//Tasks(ID, IDNode, Evacuation Rate, Evacuation Date)
				tasks.add(new Task(i, Integer.parseInt(tasksinfo.split(" ")[0]), Integer.parseInt(tasksinfo.split(" ")[1]), Integer.parseInt(tasksinfo.split(" ")[2])));
				//Number of people to evacuate
				tasks.get(i).setNbPeople(nbpeople);
				//Duration of the task : NumberofPeople / EvacuationRate
				tasks.get(i).setDuration();
				//System.out.println("task:"+tasks[i-2].id+" "+ tasks[i-2].evacdate + " idnode=" + tasks[i-2].idnode+ " " + nbpeople);
			}
			return tasks;
		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", solutionpath);
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Check if the tasks of the resource doesn't overload the capacity
	 */
	public boolean checkResource(Resource resource) {
		double use = 0;
		double capacity = resource.getCapacity();
		ArrayList<Task> rtasks = resource.getTasks();
		int t_start = Integer.MAX_VALUE;
		int t_end = -1;
		// Initialization of : t_start start of the earliest task, t_end end of the
		// latest task
		for (Task t : rtasks) {
			if (t.getShift() < t_start) {
				t_start = t.getShift();
				if(Checker.debug) {System.out.println("New t_start = "+t_start+", E"+(t.getId()+1));}
			}
			if (t.getEvactime() > t_end) {
				t_end = (int) (t.getEvactime());
				if(Checker.debug) {System.out.println("New t_end= "+t_end+", E"+(t.getId()+1));}
			}
		}
		if(Checker.debug) {System.out.println("We test for the resource R"+(resource.getId()+1)+" of capacity :" +capacity);}
		// Start checkerying the resource :
		for (int i = t_start; i <= t_end; i++) {
			// Check if any task started or ended
			//if(debug) {System.out.println("t=" + i + " use=" + use);}
			for (Task t : rtasks) {
				if (t.getShift() == i) {
					use += (double) t.getRate();
					if(debug) {System.out.println("Task E" + (t.getId() + 1) + " starts (rate="+t.getRate()+") at node:"+t.getIdnode()+" at t="+i);}
					}
				if (t.getEvactime() == i) {
					use -= (double) t.getRate();
					if(debug) {System.out.println("Task E" + (t.getId() + 1) + " ends (rate="+t.getRate()+") at node:"+t.getIdnode()+" at t="+i);}
				}
			}

			// Check if the capacity has been breached
			if (use > capacity) {
				if(Checker.debug) {
					for (Task t : Read.tasks)	{
						System.out.println("Task E"+t.getId()+" node: "+t.getIdnode());
					}
					System.err.format("Error: Resource capacity overloaded.\n");					
				}
				return false;
			}

		}
		// If everything went according to plan
		return true;
	}
	
	public boolean checkDuration() {
		double max = Read.tasks.get(0).getShift()+Read.tasks.get(0).getDuration();
		for (Task t : Read.tasks)	{
			if (t.getShift()+t.getDuration() > max)	{
				max = t.getShift()+t.getDuration();
			}
			//System.out.println("Task: E"+(t.getId()+1)+", node: "+t.getIdnode()+", shift: "+t.getShift()+", duration: "+t.getDuration());
		}

		int escapeTime = 0;
		try {
			List<String> lines = Files.readAllLines(Paths.get(solutionpath));
			escapeTime = new Integer(lines.get(new Integer(lines.get(1))+3));
		} catch (IOException e) {
			//System.err.format("Exception occurred trying to read '%s'.", solutionpath);
			e.printStackTrace();
		}

		if (escapeTime == max)	{
			System.out.println("Escape time is valid!");
			return true;
		} else {
			System.err.format("Escape time is invalid!\n");
			return false;			
		}
	}
	
}