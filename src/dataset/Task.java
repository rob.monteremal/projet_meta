package dataset;

public class Task {
	private int id;
	private int idnode;
	private int rate;
	private int shift;
	private int nbPeople;
	private double duration;

	public Task(int id, int idnode, int rate, int shift) {
		this.id= id;
		this.idnode = idnode;
		this.shift = shift;
		this.rate= rate;
	}

	public Task(Task t) {
		this.id = t.getId();
		this.idnode = t.getIdnode();
		this.setShift(t.getShift());
		this.rate= t.getRate();
		this.duration = t.getDuration();
	}
	
	public void incrementShift(int shift) {
		this.shift += shift;
//        System.out.println("E"+(this.getId()+1)+" increment: "+shift);
	}

	/**
	 * @return the nbPeople
	 */
	public int getNbPeople() {
		return nbPeople;
	}

	/**
	 * @return the shift
	 */
	public int getShift() {
		return shift;
	}
	
	public int getRate() {
		return rate;
	}
	/**
	 * @return the duration
	 */
	public double getDuration() {
		return duration;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * @return the idnode
	 */
	public int getIdnode() {
		return this.idnode;
	}
	
	/**
	 * @return the total evacuation time
	 */
	public double getEvactime() {
		return this.getDuration()+this.getShift();
	}
	
	
	/**
	 * @param idnode the idnode to set
	 */
	public void setIDNode(int idnode) {
		this.idnode = idnode;
	}

	/**
	 * @param nbPeople the nbPeople to set
	 */
	public void setNbPeople(int nbPeople) {
		this.nbPeople = nbPeople;
	}

	/**
	 * @param shift the shift to set
	 */
	public void setShift(int shift) {
		this.shift = shift;
//        System.out.println("E"+(this.getId()+1)+" set "+shift);
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration() {
		this.duration = Math.ceil((double)this.nbPeople/(double)this.rate);
	}
}
