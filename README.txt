=== CHECKER ===
To use the checker in our projet, simply call in the Main the constructor of the class Checker :
new Checker(String SolutionPath);
SolutionPath being the path of the solution you are trying to check.

The Checker will then read the corresponding instance (you need to have it in the Instances file) and tell you if the solution is valid, 
or if it isn't, and if it isn't, will tell you what's the problem.


=== LOWERBOUND and UPPERBOUND ===
To get the lower bound and upper bound of an instance, simply call the static function computeLowerBound and computeUpperBound of the Read class.
Read.computeLowerBound(String InstancePath);
Read.computeUpperBound(String InstancePath);
InstancePath being the path of the instance you're trying to get the bounds of.


=== INTENSIFICATION ===
To get a solution using our algorithm, simply create a new Solution using :
Solution.FindSolutionSwap(String InstancePath);
InstancePath being the path of the instance you're trying to get the solution of.
You can then display this solution in the console using the fonction :
Solution printSolution();
And you can then create a solution file using the method :
Solution writeToFile(String Prefixname);
Prefixname being the prefix you want to put before the file name, the file will be named "Prefixname"+"Instancename".

For exemple :
Solution solution = Solution.FindSolutionSwap("Instances/jeutest.full");
solution.printSolution();
solution.writeToFile("neighbor_swap");

The solution file will be named "neighbor_swap_jeutest.full" and will be saved in the solutions directory.